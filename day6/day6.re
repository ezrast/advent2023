let lines = In_channel.input_lines(stdin);

let (times, distances) = switch lines {
| [time_line, distance_line] =>
  let times = switch (time_line |> String.split_on_char(':')) {
  | ["Time", nums_str] => nums_str |> String.split_on_char(' ') |> List.filter((!=)("")) |> List.map(int_of_string)
  | _ => failwith("Bad input")
  };
  let distances = switch (distance_line |> String.split_on_char(':')) {
  | ["Distance", nums_str] => nums_str |> String.split_on_char(' ') |> List.filter((!=)("")) |> List.map(int_of_string)
  | _ => failwith("Bad input")
  };
  (times, distances)
| _ => failwith("Bad input")
}

// given t seconds, d distance, find n such that:
// n(t - n) > d

// Presumably, n(t - n) = d has 2 real solutions for all inputs, so that winning the race is possible
// nt - n^2 = d
// n^2 - nt + d = 0
// n = (t +- sqrt(t^2 - 4d)) / 2

let winning_options = List.map2((seconds_int, distance_int) => {
  let seconds = float_of_int(seconds_int)
  let distance = float_of_int(distance_int)

  let sqrt_term = Float.sqrt(seconds *. seconds -. 4. *. distance)
  let f1 = (seconds -. sqrt_term) /. 2.
  let f2 = (seconds +. sqrt_term) /. 2.

  // This algorithm finds ties as well as wins, so use succ/pred to shrink the range slightly before converting to ints.
  // Interestingly, my input doesn't contain an edge case where this matters, but the sample input does.
  let n1 = int_of_float(Float.ceil(Float.succ(f1)))
  let n2 = int_of_float(Float.pred(f2))

  // Before adding succ/pred to the above, I used the following to shrink the range when appropriate.
  // It also checks an extra value in each direction to defend against more glaring errors.

  // let check_win(charge_time) = {
  //   charge_time * (seconds_int - charge_time) > distance_int
  // }

  // let n1 = switch (check_win(n1 - 1), check_win(n1), check_win(n1 + 1)) {
  // | (false, false, true) => n1 + 1
  // | (false, true, true) => n1
  // | _ => failwith("Math isn't real")
  // }
  // let n2 = switch (check_win(n2 - 1), check_win(n2), check_win(n2 + 1)) {
  // | (true, false, false) => n2 - 1
  // | (true, true, false) => n2
  // | _ => failwith("The quadratic formula is a sham")
  // }

  n2 - n1 + 1
})

let winning_options_product = winning_options(times, distances) |> List.fold_left((*), 1)
print_endline("Winning options product: " ++ string_of_int(winning_options_product))

// Part 2

let (times2, distances2) = switch lines {
| [time_line, distance_line] =>
  let time = switch (time_line |> String.split_on_char(':')) {
  | ["Time", nums_str] => nums_str |> String.to_seq |> Seq.filter((!=)(' ')) |> String.of_seq |> int_of_string
  | _ => failwith("Bad input")
  };
  let distance = switch (distance_line |> String.split_on_char(':')) {
  | ["Distance", nums_str] => nums_str |> String.to_seq |> Seq.filter((!=)(' ')) |> String.of_seq |> int_of_string
  | _ => failwith("Bad input")
  };
  ([time], [distance])
| _ => failwith("Bad input")
}

let winning_options_product2 = winning_options(times2, distances2) |> List.hd
print_endline("Winning options 2: " ++ string_of_int(winning_options_product2))
