let is_valid_pull(pull: Input.pull) = {
  switch pull {
  | Red(num) => num <= 12
  | Green(num) => num <= 13
  | Blue(num) => num <= 14
  }
}

let is_valid_game(game: Input.game) = {
  List.for_all(is_valid_pull, game.pulls)
}

let go() = {
  let sum = Input.games
  |> List.filter(is_valid_game)
  |> List.map((game: Input.game) => game.game_id)
  |> List.fold_left((+), 0)
  print_endline(string_of_int(sum))
}
