let power(game: Input.game) = {
  // could do a fancy fold here, but imperative style is quicker for me *shrug*
  let red = ref(0)
  let green = ref(0)
  let blue = ref(0)

  game.pulls |> List.iter((pull: Input.pull) => {
    switch pull {
    | Red(num) when num > red^ => red := num
    | Green(num) when num > green^ => green := num
    | Blue(num) when num > blue^ => blue := num
    | _ => ()
    }
  })

  red^ * blue^ * green^
}

let go() = {
  let sum = Input.games
  |> List.map(power)
  |> List.fold_left((+), 0)
  print_endline(string_of_int(sum))
}
