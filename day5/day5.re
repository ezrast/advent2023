let lines = In_channel.input_lines(stdin);

module StringMap = Map.Make(String);

module Parser = {
  type parser_state = {
    seed_ids: list(int),
    src_type: option(string),
    type_conv_map: StringMap.t(string),
    id_conv_map: StringMap.t(int => int),
    breakpoints: StringMap.t(list(int)),
  };

  let init_parser_state = {
    seed_ids: [],
    src_type: None,
    type_conv_map: StringMap.empty,
    id_conv_map: StringMap.empty,
    breakpoints: StringMap.empty,
  };

  let rec loop(state, lines) = {
    switch lines {
    | ["", ...rest] => loop(state, rest)
    | [] => state
    | [line, ...rest] =>
      switch (String.split_on_char(':', line)) {
      | ["seeds", num_list] =>
        loop({...state, seed_ids: num_list |> String.split_on_char(' ') |> List.filter((!=)("")) |> List.map(int_of_string)}, rest)
      | [conv_types_line, ""] =>
        switch (String.split_on_char(' ', conv_types_line)) {
        | [conv_types, "map"] =>
          switch (String.split_on_char('-', conv_types)) {
          | [src, "to", dest] =>
            loop({
              ...state,
              src_type: Some(src),
              type_conv_map: state.type_conv_map |> StringMap.add(src, dest),
            }, rest)
          | _ => failwith("Bad line: " ++ line)
          }
        | _ => failwith("Bad line: " ++ line)
        }
      | [conv_ranges] =>
        switch (conv_ranges |> String.split_on_char(' ') |> List.map(int_of_string)) {
        | [dest_range_start, src_range_start, length] =>
          let src_type = state.src_type |> Option.get
          let old_fn = state.id_conv_map |> StringMap.find_opt(src_type) |> Option.value(~default=Fun.id)
          let new_fn(idx) = {
            let offset = idx - src_range_start
            if (offset >= 0 && offset < length) {
              dest_range_start + offset
            } else {
              old_fn(idx)
            }
          }
          let old_breakpoints = state.breakpoints |> StringMap.find_opt(src_type) |> Option.value(~default=[])
          let new_breakpoints = [src_range_start, src_range_start + length, ...old_breakpoints]
          loop({
            ...state,
            id_conv_map: StringMap.add(src_type, new_fn, state.id_conv_map),
            breakpoints: StringMap.add(src_type, new_breakpoints, state.breakpoints),
          }, rest)
        | exception _ | _ => failwith("Bad line: " ++ line)
        }
      | _ => failwith("Bad line: " ++ line)
      }
    }
  }
  let parse(lines) = loop(init_parser_state, lines)
}

let Parser.{seed_ids, breakpoints, type_conv_map, id_conv_map, _} = Parser.parse(lines)

let lookup_id(id: int, kind: string) = {
  StringMap.find(kind, id_conv_map)(id);
}

let lookup_kind(kind: string) = {
  StringMap.find(kind, type_conv_map);
}

let rec trace_to(id, kind, dest) = {
  if (kind == dest) {
    id
  } else {
    trace_to(lookup_id(id, kind), lookup_kind(kind), dest)
  }
}

// Part one

let location_ids = seed_ids |> List.map(seed_id => trace_to(seed_id, "seed", "location"))
let min_location = location_ids |> List.fold_left(Int.min, max_int)
print_endline("Minimum location: " ++ string_of_int(min_location))

// Part two
let rec convert_to_ranges(ids) = {
  switch ids {
  | [] => []
  | [start, length, ...rest] => [(start, length), ...convert_to_ranges(rest)]
  | _ => failwith("Odd number of seed inputs")
  }
}

let seed_ranges = convert_to_ranges(seed_ids)

let rec trace_ranges_to(ranges, kind, dest) = {
  if (kind == dest) {
    ranges
  } else {
    let kind_breakpoints = breakpoints |> StringMap.find(kind)
    let rec break_ranges(ranges) = {
      switch ranges {
      | [] => []
      | [(start, length), ...rest] =>
        switch (kind_breakpoints |> List.find_opt(bp => bp > start && bp < (start + length - 1))) {
        | Some(bp) => break_ranges([(start, bp - start), (bp, start + length - bp), ...rest])
        | None => [(start, length), ...break_ranges(rest)]
        }
      }
    }
    let broken_ranges = break_ranges(ranges)
    let converted_ranges = broken_ranges |> List.map(((start, length)) => {
      let new_start = lookup_id(start, kind);
      (new_start, length)
    });
    let new_kind = lookup_kind(kind)
    trace_ranges_to(converted_ranges, new_kind, dest)
  }
}

let location_ranges = trace_ranges_to(seed_ranges, "seed", "location")
let min_location2 = location_ranges |> List.fold_left((acc, (start, _length)) => Int.min(acc, start), max_int)
print_endline("Minimum location 2: " ++ string_of_int(min_location2))
