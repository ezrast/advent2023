let is_letter(char) = (char >= 'A' && char <= 'Z')

type turn = ((string, string)) => string

let parse(lines) = switch lines {
| [turns_str, "", ...rest] =>
  let turns: Seq.t(turn) = turns_str
  |> String.to_seq
  |> Seq.map(char => switch char { |'L' => fst | 'R' => snd | _ => failwith("Bad turn input") })
  |> Seq.cycle
  |> Seq.take(10_000_000_000);

  let nodes = Hashtbl.create(32);

  rest |> List.iter(line => {
    let disp = line |> String.to_seq |> Seq.to_dispenser;
    let rec tokenize(tokens, current_word) = {
      switch (disp()) {
      | Some(' ') => tokenize(tokens, current_word)
      | Some(letter) when is_letter(letter) => tokenize(tokens, Seq.cons(letter, current_word))
      | Some(symbol) =>
        switch (String.of_seq(current_word)) {
        | "" => tokenize([`Sym(symbol), ...tokens], Seq.empty)
        // Words come out reversed but as long as it happens consistently we don't care
        | word => tokenize([`Sym(symbol), `Word(word), ...tokens], Seq.empty)
        }
      | None =>
        switch (String.of_seq(current_word)) {
        | "" => tokens
        | word => [`Word(word), ...tokens]
        }
      }
    }

    switch (tokenize([], Seq.empty)) {
    // Tokens come out reversed
    | [`Sym(')'), `Word(dest_snd), `Sym(','), `Word(dest_fst), `Sym('('), `Sym('='), `Word(src)] =>
      Hashtbl.add(nodes, src, (dest_fst, dest_snd))
    | _ => failwith("Bad line: " ++ line)
    }
  });

  (turns, nodes)
| _ => failwith("Bad input")
};

let rec count(num, node_tbl, location, turns: Seq.t(turn)) = {
  if (location == "ZZZ") num
  else {
    let (turn, new_turns) = switch (Seq.uncons(turns)) {
    | Some(tuple) => tuple
    | None => failwith("Too many turns")
    }
    switch (Hashtbl.find_opt(node_tbl, location)) {
    | Some(node) => count(num + 1, node_tbl, node |> turn, new_turns)
    | None => failwith("Bad turn at: " ++ location)
    }
  }
}

let run(lines) = {
  let (turns: Seq.t(turn), nodes) = parse(lines)
  print_endline("Turns taken: " ++ string_of_int(count(0, nodes, "AAA", turns)))
}
