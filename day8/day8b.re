let is_word_char(char) = (char >= 'A' && char <= 'Z') || (char >= '0' && char <= '9')

let rec gcd(aa, bb) = {
  if (bb == 0) aa
  else gcd(bb, aa mod bb);
}

let lcm(aa, bb) = {
  if (aa > bb) {
    (aa / gcd(aa, bb)) * bb
  } else {
    (bb / gcd(bb, aa)) * aa
  }
}

type node = {
  mutable turns: array(node),
  name: string,
  left_name: string,
  right_name: string,
  end_z: bool,
  end_a: bool,
}

let parse(lines) = switch lines {
| [turns_str, "", ...rest] =>
  let turns = turns_str
  |> String.to_seq
  |> Seq.map(char => switch char { |'L' => 0 | 'R' => 1 | _ => failwith("Bad turn input") })
  |> Array.of_seq;

  let node_tbl = Hashtbl.create(32);

  rest |> List.iter(line => {
    let disp = line |> String.to_seq |> Seq.to_dispenser;
    let rec tokenize(tokens, current_word) = {
      switch (disp()) {
      | Some(' ') => tokenize(tokens, current_word)
      | Some(letter) when is_word_char(letter) => tokenize(tokens, Seq.cons(letter, current_word))
      | Some(symbol) =>
        switch (String.of_seq(current_word)) {
        | "" => tokenize([`Sym(symbol), ...tokens], Seq.empty)
        // Words come out reversed but as long as it happens consistently we don't care
        | word => tokenize([`Sym(symbol), `Word(word), ...tokens], Seq.empty)
        }
      | None =>
        switch (String.of_seq(current_word)) {
        | "" => tokens
        | word => [`Word(word), ...tokens]
        }
      }
    }

    switch (tokenize([], Seq.empty)) {
    // Tokens come out reversed
    | [`Sym(')'), `Word(right_name), `Sym(','), `Word(left_name), `Sym('('), `Sym('='), `Word(name)] =>
      node_tbl |> Hashtbl.add(_, name, {
        turns: [||],
        name,
        left_name,
        right_name,
        end_z: String.get(name, 0) == 'Z', // index 0 is the end because words are reversed
        end_a: String.get(name, 0) == 'A',
      })
    | _ => failwith("Bad line: " ++ line)
    }
  });

  node_tbl |> Hashtbl.iter((_key, node) => {
    let left_node = node.left_name |> Hashtbl.find(node_tbl);
    let right_node = node.right_name |> Hashtbl.find(node_tbl);
    node.turns = [|left_node, right_node|]
  });

  (turns, node_tbl)
| _ => failwith("Bad input")
};

module IntSet = Set.Make(Int)

module Cycle = {
  type t = {
    offset: int,
    repeat_length: int,
    z_node_idxs: IntSet.t,
  }

  let null = {
    offset: 0,
    repeat_length: 1,
    z_node_idxs: IntSet.empty |> IntSet.add(1),
  }

  let is_z_node(idx, cycle) = {
    let idx = if (idx < cycle.offset) {
      idx
    } else {
      cycle.offset + ((idx - cycle.offset) mod cycle.repeat_length)
    }
    IntSet.mem(idx, cycle.z_node_idxs)
  }

  let merge(aa, bb) = {
    let offset = Int.max(aa.offset, bb.offset);
    let new_repeat_length = lcm(aa.repeat_length, bb.repeat_length);
    let merged_z_idxs = ref(IntSet.empty)
    let (aa_head_node_idxs, aa_repeat_node_idxs) = aa.z_node_idxs |> IntSet.partition(idx => idx < aa.offset)
    aa_head_node_idxs |> IntSet.iter(idx => {
      if (is_z_node(idx, bb)) {
        merged_z_idxs := merged_z_idxs^ |> IntSet.add(idx)
      }
    });

    aa_repeat_node_idxs |> IntSet.iter(base_idx => {
      for (repeat_count in 0 to (new_repeat_length / aa.repeat_length) - 1) {
        let idx = base_idx + aa.repeat_length * repeat_count
        if (is_z_node(idx, bb)) {
          merged_z_idxs := merged_z_idxs^ |> IntSet.add(idx)
        }
      }
    });

    {
      offset,
      repeat_length: new_repeat_length,
      z_node_idxs: merged_z_idxs^,
    }
  }
}


module GraphPosition = {
  type t = {
    turn_idx: int,
    node_name: string,
  }

  let compare(aa, bb) = {
    if (aa.turn_idx == bb.turn_idx) {
      String.compare(aa.node_name, bb.node_name)
    } else {
      Int.compare(aa.turn_idx, bb.turn_idx)
    }
  }
}

module VisitedMap = Map.Make(GraphPosition)

let find_cycle(turns, starting_node) = {
  let rec loop(visited_map, visited_z_list, current_node, turn_count) = {
    let turn_idx = turn_count mod Array.length(turns);
    let current_pos = GraphPosition.{turn_idx, node_name: current_node.name};
    switch (VisitedMap.find_opt(current_pos, visited_map)) {
    | Some(branch_idx) =>
      Cycle.{
        offset: branch_idx,
        repeat_length: turn_count - branch_idx,
        z_node_idxs: visited_z_list |> IntSet.of_list
      }
    | None =>
      loop(
        visited_map |> VisitedMap.add(current_pos, turn_count),
        if (current_node.end_z) [turn_count, ...visited_z_list] else visited_z_list,
        current_node.turns[turns[turn_idx]],
        turn_count + 1
      )
    }
  };
  loop(VisitedMap.empty, [], starting_node, 0)
}

let run(lines) = {
  let (turns, node_tbl) = parse(lines)

  let locations = node_tbl |> Hashtbl.to_seq_values |> Seq.filter(loc => loc.end_a) |> List.of_seq
  let cycles = locations |> List.map(find_cycle(turns))
  let merged_cycle = cycles |> List.fold_left(Cycle.merge, Cycle.null)
  print_endline("Ghostly cycle length: " ++ string_of_int(merged_cycle.z_node_idxs |> IntSet.min_elt))
}


