let lines = In_channel.input_lines(stdin);

let array = lines
|> List.map(line => line |> String.to_seq |> Array.of_seq)
|> Array.of_list

module Ray {
  type t = {
    xx: int,
    yy: int,
    dir: int,
  }

  let compare(p1, p2) = {
    if (p1.xx == p2.xx) {
      if (p1.yy == p2.yy) {
        Int.compare(p1.dir, p2.dir)
      } else {
        Int.compare(p1.yy, p2.yy)
      }
    } else {
      Int.compare(p1.xx, p2.xx)
    }
  }
}

module Point {
  type t = {
    xx: int,
    yy: int,
  }

  let compare(p1, p2) = {
    if (p1.xx == p2.xx) {
      Int.compare(p1.yy, p2.yy)
    } else {
      Int.compare(p1.xx, p2.xx)
    }
  }
}

module PointSet = Set.Make(Point);
module RaySet = Set.Make(Ray);

let get(arr, xx, yy) = {
  if (yy >= 0 && yy < Array.length(arr)) {
    let row = arr[yy]
    if (xx >= 0 && xx < Array.length(row)) {
      Some(row[xx])
    } else {
      None
    }
  } else {
    None
  }
}

// north = 0
// south = 1
// east = 2
// west = 3

let find_visited(initial_ray, arr) = {
  let calculate_pending(old_pending, Ray.{xx, yy, dir} as ray) = {
    let (next_xx, next_yy) = switch dir {
    | 0 => (xx, yy - 1)
    | 1 => (xx, yy + 1)
    | 2 => (xx + 1, yy)
    | 3 => (xx - 1, yy)
    | _ => failwith("Bad dir")
    }

    let next_dirs = switch (get(arr, next_xx, next_yy)) {
    | None => []
    | Some('/') => [(dir + 2) mod 4]  // 0 <-> 2, 1 <-> 3
    | Some('\\') => [3 - dir]  // 0 <-> 3, 1 <-> 2
    | Some('|') => if (dir <= 1) [dir] else [0, 1]
    | Some('-') => if (dir > 1) [dir] else [2, 3]
    | Some('.') => [dir]
    | _ => failwith("Bad map icon")
    }

    let new_pending = next_dirs |> List.fold_left((acc, dir) => {
      acc |> RaySet.add({xx: next_xx, yy: next_yy, dir})
    }, old_pending |> RaySet.remove(ray))
    new_pending
  }

  let visited = Array.init(4, _ => PointSet.empty)
  let rec flood_fill(pending, arr) = {
    switch (pending |> RaySet.min_elt_opt) {
    | None => ()
    | Some(({xx, yy, dir} as this_ray)) =>
      let seen = visited[dir] |> PointSet.mem({xx, yy});
      if (seen) {
        // print_endline("Seen")
        flood_fill(pending |> RaySet.remove(this_ray), arr)
      } else {
        let new_pending = calculate_pending(pending, this_ray)
        visited[dir] = visited[dir] |> PointSet.add({xx, yy})
        flood_fill(new_pending, arr)
      }

    }
  }

  flood_fill(calculate_pending(RaySet.empty, initial_ray), arr)
  visited |> Array.fold_left(PointSet.union, PointSet.empty)
}

let visited = find_visited({xx: -1, yy: 0, dir: 2}, array)

// for (yy in 0 to Array.length(array) - 1) {
//   for (xx in 0 to Array.length(array[0]) - 1) {
//     if(visited |> PointSet.mem({xx, yy})) {
//       print_char('#')
//     } else {
//       print_char(array[yy][xx])
//     }
//   }
//   print_newline()
// }

let visited_count = PointSet.cardinal(visited)
print_endline("Visited tiles: " ++ string_of_int(visited_count))

// Part two

let west_edge = Seq.ints(0) |> Seq.take(Array.length(array)) |> Seq.map(yy => Ray.{xx: -1, yy, dir: 2})
let east_edge = Seq.ints(0) |> Seq.take(Array.length(array)) |> Seq.map(yy => Ray.{xx: Array.length(array[0]), yy, dir: 3})
let north_edge = Seq.ints(0) |> Seq.take(Array.length(array[0])) |> Seq.map(xx => Ray.{xx, yy: -1, dir: 1})
let south_edge = Seq.ints(0) |> Seq.take(Array.length(array[0])) |> Seq.map(xx => Ray.{xx, yy: Array.length(array), dir: 0})

let max_visited_count = [west_edge, east_edge, north_edge, south_edge]
|> List.to_seq
|> Seq.concat
|> Seq.map(ray => find_visited(ray, array) |> PointSet.cardinal)
|> Seq.fold_left(Int.max, 0)
print_endline("Maximum visited tiles: " ++ string_of_int(max_visited_count))
