let lines = In_channel.input_lines(stdin)

let rec get_blocks(~ret=[], lines_seq) = {
  if (Seq.is_empty(lines_seq)) {
    ret
  } else {
    let block = lines_seq |> Seq.take_while((!=)("")) |> Array.of_seq
    let rest = lines_seq |> Seq.drop_while((!=)("")) |> Seq.drop(1)
    get_blocks(~ret=[block, ...ret], rest)
  }
}

let transpose(block: array(string)): array(string) = {
  Seq.ints(0) |> Seq.take(String.length(block[0])) |> Seq.map(idx => {
    block |> Array.to_seq |> Seq.map(String.get(_, idx)) |> String.of_seq
  })
  |> Array.of_seq
}

let try_get(idx, arr) = {
  if (idx < 0 || idx >= Array.length(arr)) None
  else Some(arr[idx])
}

let rec check_mirror(~offset=0, block, idx) = {
  switch (try_get(idx - 1 - offset, block), try_get(idx + offset, block)) {
  | (None, _)
  | (_, None) => true
  | (Some(aa), Some(bb)) when aa != bb => false
  | _ => check_mirror(~offset=offset + 1, block, idx)
  }
}

let rec find_symmetry(~idx=1, block) = {
  if (idx > Array.length(block) - 1) None
  else if (check_mirror(block, idx)) Some(idx)
  else find_symmetry(~idx=idx+1, block)
}

let blocks = lines |> List.to_seq |> get_blocks
let t_blocks = blocks |> List.map(transpose)

let symmetries = blocks |> List.map(block => 100 * (find_symmetry(block) |> Option.value(~default=0)));
let t_symmetries = t_blocks |> List.map(block => find_symmetry(block) |> Option.value(~default=0));

let sum = (symmetries |> List.fold_left((+), 0)) + (t_symmetries |> List.fold_left((+), 0))
print_endline("Sum: " ++ string_of_int(sum))

// Part two

let (=~)(str1, str2) = {
  assert(String.length(str1) == String.length(str2));
  let rec loop2(idx) = {
    if (idx >= String.length(str1)) `Near
    else if (String.get(str1, idx) == String.get(str2, idx)) loop2(idx + 1)
    else `No
  }
  let rec loop1(idx) = {
    if (idx >= String.length(str1)) `Equal
    else if (String.get(str1, idx) == String.get(str2, idx)) loop1(idx + 1)
    else loop2(idx + 1)
  }
  loop1(0)
}

let rec check_mirror_near(~offset=0, block, idx) = {
  switch (try_get(idx - 1 - offset, block), try_get(idx + offset, block)) {
  | (None, _)
  | (_, None) => false
  | (Some(aa), Some(bb)) =>
    switch (aa =~ bb) {
    | `No => false
    | `Near => check_mirror(~offset=offset + 1, block, idx)
    | `Equal => check_mirror_near(~offset=offset + 1, block, idx)
    }
  }
}

let rec find_near_symmetry(~idx=1, block) = {
  if (idx > Array.length(block) - 1) None
  else if (check_mirror_near(block, idx)) Some(idx)
  else find_near_symmetry(~idx=idx+1, block)
}

let near_symmetries = blocks |> List.map(block => 100 * (find_near_symmetry(block) |> Option.value(~default=0)));
let t_near_symmetries = t_blocks |> List.map(block => find_near_symmetry(block) |> Option.value(~default=0));

let sum = (near_symmetries |> List.fold_left((+), 0)) + (t_near_symmetries |> List.fold_left((+), 0))
print_endline("Near-symmetry sum: " ++ string_of_int(sum))
