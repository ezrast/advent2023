let lines = In_channel.input_lines(stdin);

module Point {
  type t = {
    xx: int,
    yy: int,
  }

  let compare(p1, p2) = {
    if (p1.xx == p2.xx) {
      Int.compare(p1.yy, p2.yy)
    } else {
      Int.compare(p1.xx, p2.xx)
    }
  }
}

module PointMap = Map.Make(Point);
module IntMap = Map.Make(Int);

let array = lines
|> List.map(line => line |> String.to_seq |> Array.of_seq)
|> Array.of_list

let get(arr, xx, yy) = {
  if (xx >= 0 && xx < Array.length(arr)) {
    let row = arr[xx]
    if (yy >= 0 && yy < Array.length(row)) {
      Some(row[yy])
    } else {
      None
    }
  } else {
    None
  }
}

let faces_north(char) = char == Some('J') || char == Some('|') || char == Some('L') || char == Some('S')
let faces_south(char) = char == Some('F') || char == Some('|') || char == Some('7') || char == Some('S')
let faces_east(char) = char == Some('F') || char == Some('-') || char == Some('L') || char == Some('S')
let faces_west(char) = char == Some('J') || char == Some('-') || char == Some('7') || char == Some('S')

let start = array
|> Array.find_mapi((xx, row) => {
  row
  |> Array.find_mapi((yy, char) => {
    if (char == 'S') Some(Point.{xx, yy}) else None
  })
})
|> Option.get

let rec flood_fill(visited: PointMap.t(int), pending, arr) = {
  switch (pending |> PointMap.min_binding_opt) {
  | None => visited
  | Some(({xx, yy}, dist)) =>
    switch (visited |> PointMap.find_opt({xx, yy})) {
    | Some(other_dist) when other_dist <= dist => flood_fill(visited, pending |> PointMap.remove({xx, yy}), arr)
    | _ =>
      // print_endline(string_of_int(xx) ++ ", " ++ string_of_int(yy) ++ ": " ++ string_of_int(dist))
      let pending = if (faces_north(get(arr, xx, yy)) && faces_south(get(arr, xx - 1, yy))) { pending |> PointMap.add({xx: xx - 1, yy}, dist + 1) } else pending;
      let pending = if (faces_south(get(arr, xx, yy)) && faces_north(get(arr, xx + 1, yy))) { pending |> PointMap.add({xx: xx + 1, yy}, dist + 1) } else pending;
      let pending = if (faces_east(get(arr, xx, yy)) && faces_west(get(arr, xx, yy + 1))) { pending |> PointMap.add({xx, yy: yy + 1}, dist + 1) } else pending;
      let pending = if (faces_west(get(arr, xx, yy)) && faces_east(get(arr, xx, yy - 1))) { pending |> PointMap.add({xx, yy: yy - 1}, dist + 1) } else pending;
      let pending = pending |> PointMap.remove({xx, yy})
      let visited = visited |> PointMap.add({xx, yy}, dist);
      flood_fill(visited, pending, arr)
    }
  }
}

let visited = flood_fill(PointMap.empty, PointMap.empty |> PointMap.add(start, 0), array)
let furthest = visited |> PointMap.to_seq |> Seq.map(snd) |> Array.of_seq
furthest |> Array.sort((aa, bb) => Int.compare(bb, aa))
print_endline("Furthest distance: " ++ string_of_int(furthest[0]))

// Part two

let Point.{xx: sx, yy: sy} = start;
switch (
  faces_north(get(array, sx + 1, sy)),
  faces_south(get(array, sx - 1, sy)),
  faces_east(get(array, sx, sy - 1)),
  faces_west(get(array, sx, sy + 1)),
) {
| (true, true, false, false) => array[sx][sy] = '|'
| (true, false, true, false) => array[sx][sy] = '7'
| (true, false, false, true) => array[sx][sy] = 'F'
| (false, true, true, false) => array[sx][sy] = 'J'
| (false, true, false, true) => array[sx][sy] = 'L'
| (false, false, true, true) => array[sx][sy] = '-'
| _ => failwith("Logic bug")
}

let count = ref(0)
array |> Array.iteri((xx, line) => {
  let state = ref(`Out)
  line |> Array.iteri((yy, char) => {
    switch (visited |> PointMap.mem({xx, yy}), state^, char) {
    | (false, `Out, _) => ()
    | (false, `In, _) => incr(count)
    | (false, _, _) => failwith("Logic bug: _ at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))

    | (true, `Out, '|') => state := `In
    | (true, `Out, 'F') => state := `Bottom_in
    | (true, `Out, 'L') => state := `Top_in
    | (true, `Out, 'J') => failwith("Logic bug: Out at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))
    | (true, `Out, '7') => failwith("Logic bug: Out at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))
    | (true, `Out, '-') => failwith("Logic bug: Out at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))
    | (true, `Out, _) => ()

    | (true, `In, '|') => state := `Out
    | (true, `In, 'F') => state := `Top_in
    | (true, `In, 'L') => state := `Bottom_in
    | (true, `In, 'J') => failwith("Logic bug: In at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))
    | (true, `In, '7') => failwith("Logic bug: In at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))
    | (true, `In, '-') => failwith("Logic bug: In at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))
    | (true, `In, _) => incr(count)

    | (true, `Top_in, '|') => failwith("Logic bug: Top_in at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))
    | (true, `Top_in, 'F') => failwith("Logic bug: Top_in at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))
    | (true, `Top_in, 'L') => failwith("Logic bug: Top_in at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))
    | (true, `Top_in, 'J') => state := `Out
    | (true, `Top_in, '7') => state := `In
    | (true, `Top_in, '-') => ()
    | (true, `Top_in, _) => failwith("Logic bug: Top_in at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))

    | (true, `Bottom_in, '|') => failwith("Logic bug: Bottom_in at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))
    | (true, `Bottom_in, 'F') => failwith("Logic bug: Bottom_in at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))
    | (true, `Bottom_in, 'L') => failwith("Logic bug: Bottom_in at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))
    | (true, `Bottom_in, 'J') => state := `In
    | (true, `Bottom_in, '7') => state := `Out
    | (true, `Bottom_in, '-') => ()
    | (true, `Bottom_in, _) => failwith("Logic bug: Bottom_in at " ++ string_of_int(xx) ++ ", " ++ string_of_int(yy))
    }
  })
})

print_endline("Enclosed area: " ++ string_of_int(count^))
