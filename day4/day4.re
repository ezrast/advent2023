let lines = In_channel.input_lines(stdin)

// thanks, Stack Overflow
let rec pow(num, exp) = {
  switch exp {
  | 0 => 1
  | 1 => num
  | nn =>
    let b = pow(num, nn / 2)
    b * b * (if (nn mod 2 == 0) 1 else num)
  }
}

module IntSet = Set.Make(Int)

let strip_header(line) = {
  line
  |> String.split_on_char(':')
  |> List.tl
  |> List.hd
}

let parse_int_set(str) = {
  str
  |> String.split_on_char(' ')
  |> List.filter((!=)(""))
  |> List.map(int_of_string)
  |> IntSet.of_list
}

let set_pairs = lines |> List.map(line => {
  switch (
    line
    |> strip_header
    |> String.split_on_char('|')
  ) {
  | [set1, set2] => (parse_int_set(set1), parse_int_set(set2))
  | _ => failwith("bad line: " ++ line)
  }
})

let match_counts = set_pairs |> List.map(((s1, s2)) => {
  s1 |> IntSet.inter(s2) |> IntSet.cardinal
})

let scores = match_counts |> List.map(count => pow(2, count) / 2)

let score_sum = scores |> List.fold_left((+), 0)
print_endline("Score: " ++ string_of_int(score_sum))

// This worked but had a run time of more than a second:
// let (total_card_count, _leftover_copies) = match_counts |> List.fold_left(((card_count, copies), match_count) => {
//   let copies_of_this_card = List.length(copies) + 1
//   let new_copies = copies |> List.map((-)(_, 1))
//   let newer_copies = Seq.iterate(list => [match_count, ...list], new_copies) |> Seq.drop(copies_of_this_card) |> Seq.uncons |> Option.get |> fst |> List.filter((!=)(0));
//   (card_count + copies_of_this_card, newer_copies)
// }, (0, []))

// circular buffer of copy counts
let copies = Array.make(1 + (match_counts |> List.fold_left(Int.max, 0)), 0)
let total_card_count = ref(0)
match_counts |> List.iteri((idx, match_count) => {
  let copies_of_this_card = Array.fold_left((+), 0, copies) + 1
  total_card_count := total_card_count^ + copies_of_this_card

  // increment the cell that will get cleared `match_count` iterations from now
  let copies_idx = (idx + match_count) mod Array.length(copies)
  copies[copies_idx] = copies[copies_idx] + copies_of_this_card

  copies[idx mod Array.length(copies)] = 0
})

print_endline("Card Count: " ++ string_of_int(total_card_count^))
