let lines = In_channel.input_lines(stdin);

let diffs(arr) = {
  Array.init(Array.length(arr) - 1, idx => {
    arr[idx + 1] - arr[idx]
  })
}

let rec predict(arr) = {
  if (Array.for_all((==)(0), arr)) 0
  else {
    arr[Array.length(arr) - 1] + predict(diffs(arr))
  }
}

let predictions = lines |> List.map(line => {
  line |> String.split_on_char(' ') |> List.map(int_of_string) |> Array.of_list |> predict
})
let sum = predictions |> List.fold_left((+), 0)
print_int(sum)
print_newline()

let rec predict_back(arr) = {
  if (Array.for_all((==)(0), arr)) 0
  else {
    arr[0] - predict_back(diffs(arr))
  }
}

let back_predictions = lines |> List.map(line => {
  line |> String.split_on_char(' ') |> List.map(int_of_string) |> Array.of_list |> predict_back
})
let sum = back_predictions |> List.fold_left((+), 0)
print_int(sum)
print_newline()
