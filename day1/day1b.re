let rec loop_fwd(line) = {
  if (String.starts_with(~prefix="zero", line) || String.starts_with(~prefix="0", line)) { 0 }
  else if (String.starts_with(~prefix="one", line) || String.starts_with(~prefix="1", line)) { 1 }
  else if (String.starts_with(~prefix="two", line) || String.starts_with(~prefix="2", line)) { 2 }
  else if (String.starts_with(~prefix="three", line) || String.starts_with(~prefix="3", line)) { 3 }
  else if (String.starts_with(~prefix="four", line) || String.starts_with(~prefix="4", line)) { 4 }
  else if (String.starts_with(~prefix="five", line) || String.starts_with(~prefix="5", line)) { 5 }
  else if (String.starts_with(~prefix="six", line) || String.starts_with(~prefix="6", line)) { 6 }
  else if (String.starts_with(~prefix="seven", line) || String.starts_with(~prefix="7", line)) { 7 }
  else if (String.starts_with(~prefix="eight", line) || String.starts_with(~prefix="8", line)) { 8 }
  else if (String.starts_with(~prefix="nine", line) || String.starts_with(~prefix="9", line)) { 9 }
  else loop_fwd(String.sub(line, 1, String.length(line) - 1))
}

let rec loop_rev(line) = {
  if (String.ends_with(~suffix="zero", line) || String.ends_with(~suffix="0", line)) { 0 }
  else if (String.ends_with(~suffix="one", line) || String.ends_with(~suffix="1", line)) { 1 }
  else if (String.ends_with(~suffix="two", line) || String.ends_with(~suffix="2", line)) { 2 }
  else if (String.ends_with(~suffix="three", line) || String.ends_with(~suffix="3", line)) { 3 }
  else if (String.ends_with(~suffix="four", line) || String.ends_with(~suffix="4", line)) { 4 }
  else if (String.ends_with(~suffix="five", line) || String.ends_with(~suffix="5", line)) { 5 }
  else if (String.ends_with(~suffix="six", line) || String.ends_with(~suffix="6", line)) { 6 }
  else if (String.ends_with(~suffix="seven", line) || String.ends_with(~suffix="7", line)) { 7 }
  else if (String.ends_with(~suffix="eight", line) || String.ends_with(~suffix="8", line)) { 8 }
  else if (String.ends_with(~suffix="nine", line) || String.ends_with(~suffix="9", line)) { 9 }
  else {
    loop_rev(String.sub(line, 0, String.length(line) - 1))
  }
}

let go(lines) = {
  let numbers = lines |> List.map(line => {
    try (loop_rev(line) + 10 * loop_fwd(line)) {
    | Invalid_argument(err_str) => failwith("Invalid_argument on " ++ line ++ ": " ++ err_str)
    }
  })

  let total = List.fold_left((+), 0, numbers)
  // numbers |> List.iter(num => print_endline(string_of_int(num)))
  print_endline("Total: " ++ string_of_int(total))
}
