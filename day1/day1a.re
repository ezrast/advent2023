
let is_digit(char) = {
  char >= '0' && char <= '9'
}

let int_of_digit(char) = {
  Char.code(char) - Char.code('0')
}

let line_to_number(line) = {
  let rec loop_rev(idx) = {
    let chr = String.get(line, idx)
    if (is_digit(chr)) {
      int_of_digit(chr)
    } else {
      loop_rev(idx - 1)
    }
  }

  let rec loop_fwd(idx) = {
    let chr = String.get(line, idx)
    if (is_digit(chr)) {
      10 * int_of_digit(chr) + loop_rev(String.length(line) - 1)
    } else {
      loop_fwd(idx + 1)
    }
  }

  loop_fwd(0)
}

let go(lines) = {
  let numbers = lines |> List.map(line_to_number)
  let total = List.fold_left((+), 0, numbers)
  print_endline("Total: " ++ string_of_int(total))
}
