// I didn't like my first solution so I made another one where I try to
// build a proper graph of number/symbol nodes instead of relying on set
// uniqueness to deduplicate numbers. I'm not sure it's any better.
let lines = In_channel.input_lines(stdin) |> Array.of_list

module Point {
  type t = {
    xx: int,
    yy: int,
  }

  let compare(p1, p2) = {
    if (p1.xx == p2.xx) {
      Int.compare(p1.yy, p2.yy)
    } else {
      Int.compare(p1.xx, p2.xx)
    }
  }
}

// convenience sequence for iterating over every point
let all_coords = lines
|> Array.to_seqi
|> Seq.map(((xx, line)) => {
  line
  |> String.to_seq
  |> Seq.mapi((yy, char) => {
    (Point.{ xx, yy }, char)
  })
})
|> Seq.concat

// Find number start/ends by taking coordinates of all digits, offsetting by 1, and taking the set difference in both directions
// e.g. "...111..."
//          xxx     <- digit_coords_set
//           xxx    <- digit_coords_offset_set
//          x       <- number starts here
//             x    <- number ends here
module PointSet = Set.Make(Point);

let digit_coords_seq = all_coords
|> Seq.filter_map(((point, char)) => if (char >= '0' && char <= '9') { Some(point) } else { None })

let digit_coords_set = digit_coords_seq |> PointSet.of_seq
let digit_coords_offset_set = digit_coords_seq
|> Seq.map((point) => Point.{ xx: point.xx, yy: point.yy + 1 })
|> PointSet.of_seq

let num_starts = PointSet.diff(digit_coords_set, digit_coords_offset_set)
let num_ends = PointSet.diff(digit_coords_offset_set, digit_coords_set)

// Build the graph of symbol and number nodes
type num_node = {
  num: int,
  links: list(Point.t),
}

and sym_node = {
  sym_point: Point.t,
  char: char,
  num_links: list(int)
};

module PointMap = Map.Make(Point);

let unlinked_symbol_map = all_coords
|> Seq.filter_map(((sym_point, char)) => {
  if (char != '.' && (char < '0' || char > '9')) {
    Some((sym_point, { sym_point, char, num_links: [] }))
  } else {
    None
  }
})
|> PointMap.of_seq

let (symbol_map, num_map) = Seq.fold_left2((acc, start: Point.t, stop: Point.t) => {
  assert(start.xx == stop.xx)
  let (old_symbol_map, old_number_map) = acc;
  let length = stop.yy - start.yy
  let num_string = lines[start.xx] |> String.sub(_, start.yy, length);
  let num = int_of_string(num_string);

  let surrounding_points = Seq.concat([
    Seq.ints(start.yy - 1) |> Seq.take(length + 2) |> Seq.map(yy => Point.{ xx: start.xx - 1, yy }),
    Seq.return(Point.{ xx: start.xx, yy: start.yy - 1}),
    Seq.return(Point.{ xx: start.xx, yy: stop.yy}),
    Seq.ints(start.yy - 1) |> Seq.take(length + 2) |> Seq.map(yy => Point.{ xx: start.xx + 1, yy }),
  ] |> List.to_seq)

  let surrounding_symbol_points = surrounding_points |> Seq.filter(PointMap.mem(_, old_symbol_map))
  let surrounding_symbols = surrounding_symbol_points |> Seq.map(PointMap.find(_, old_symbol_map))
  let new_symbol_map = surrounding_symbols |> Seq.fold_left((symbol_map, symbol: sym_node) => {
    let new_symbol = { ...symbol, num_links: [num, ...symbol.num_links] };
    symbol_map |> PointMap.add(symbol.sym_point, new_symbol)
  }, old_symbol_map)

  let new_number_map = old_number_map |> PointMap.add(start, {num, links: surrounding_symbol_points |> List.of_seq});
  (new_symbol_map, new_number_map)
}, (unlinked_symbol_map, PointMap.empty), PointSet.to_seq(num_starts), PointSet.to_seq(num_ends))

let part_numbers = num_map |> PointMap.to_seq |> Seq.filter_map(((_point, num_node)) => {
  if (num_node.links |> List.is_empty) {
    None
  } else {
    Some(num_node.num)
  }
})
let part_numbers_sum = part_numbers |> Seq.fold_left((+), 0)
print_endline("Part numbers: " ++ string_of_int(part_numbers_sum))

let gear_ratios = symbol_map |> PointMap.to_seq |> Seq.filter_map(((_point, sym_node)) => {
  switch sym_node {
  | { num_links: [aa, bb], char: '*', _ } => Some(aa * bb)
  | _ => None
  }
})
let gear_ratios_sum = gear_ratios |> Seq.fold_left((+), 0)
print_endline("Gear ratios: " ++ string_of_int(gear_ratios_sum))
