let far_rows = ref([])
let far_cols = ref([])

let lines = In_channel.input_lines(stdin)
|> List.to_seq
|> Seq.map(String.to_seq);

lines
|> Seq.iteri((idx, line) => {
  if (line |> Array.of_seq |> Array.for_all((==)('.'))) {
    far_rows := [idx, ...far_rows^]
  }
});

lines
|> Seq.transpose
|> Seq.iteri((idx, line) => {
  if (line |> Array.of_seq |> Array.for_all((==)('.'))) {
    far_cols := [idx, ...far_cols^]
  }
})

let points = ref([])

lines |> Seq.iteri((xx, line) => {
  line |> Seq.iteri((yy, char) => {
    if (char == '#') {
      points := [(xx, yy), ...points^]
    }
  })
})

let seq = points^ |> List.to_seq
let sum(stretch) = seq
|> Seq.product(seq)
|> Seq.map((((x1, y1), (x2, y2))) => {
  let (x1, x2) = if (x1 > x2) (x2, x1) else (x1, x2);
  let (y1, y2) = if (y1 > y2) (y2, y1) else (y1, y2);
  (x2 - x1) + (y2 - y1)
  + (stretch - 1) * (far_rows^ |> List.filter(idx => { x1 < idx && x2 > idx }) |> List.length)
  + (stretch - 1) * (far_cols^ |> List.filter(idx => { y1 < idx && y2 > idx }) |> List.length)
})
|> Seq.fold_left((+), 0)

print_endline("2x Sum: " ++ string_of_int(sum(2) / 2))
print_endline("1e6x Sum: " ++ string_of_int(sum(1_000_000) / 2))
