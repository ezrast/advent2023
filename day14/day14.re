let block = In_channel.input_lines(stdin)
|> List.to_seq
|> Seq.map(str => str |> String.to_seq |> Array.of_seq)
|> Array.of_seq

let shift_in_place(dir, block) = {
  let (get_main, set_main, main_dim_len, sub_dim_len) = switch dir {
  | `West =>
    (
      (main, sub) => block[main][sub],
      (main, sub, value) => block[main][sub] = value,
      Array.length(block),
      Array.length(block[0])
    )
  | `East =>
    (
      (main, sub) => block[main][Array.length(block[0]) - sub - 1],
      (main, sub, value) => block[main][Array.length(block[0]) - sub - 1] = value,
      Array.length(block),
      Array.length(block[0])
    )
  | `North =>
    (
      (main, sub) => block[sub][main],
      (main, sub, value) => block[sub][main] = value,
      Array.length(block[0]),
      Array.length(block)
    )
  | `South =>
    (
      (main, sub) => block[Array.length(block) - sub - 1][main],
      (main, sub, value) => block[Array.length(block) - sub - 1][main] = value,
      Array.length(block[0]),
      Array.length(block)
    )
  }

  for (main_idx in 0 to (main_dim_len - 1)) {
    let get = get_main(main_idx)
    let set = set_main(main_idx)
    let rec loop(sub_idx) = {
      if (sub_idx + 1 < sub_dim_len) {
        if (get(sub_idx) == '.' && get(sub_idx + 1) == 'O') {
          set(sub_idx, 'O')
          set(sub_idx + 1, '.')
          loop(Int.max(sub_idx - 1, 0))
        } else {
          loop(sub_idx + 1)
        }
      }
    }
    loop(0)
  }
}

let weight(block) = {
  block |> Array.mapi((main_idx, arr) => {
    let len = Array.length(arr)
    let rec loop(idx, sum) = {
      if (idx >= len) {
        sum
      } else {
        if (arr[idx] == 'O') loop(idx + 1, sum + (Array.length(block) - main_idx))
        else loop(idx + 1, sum)
      }
    }
    loop(0, 0)
  })
  |> Array.fold_left((+), 0)
}

shift_in_place(`North, block)
print_endline("Weight: " ++ string_of_int(weight(block)))

// Day 2

// All the points a boulder can be piled up on after an eastward shift
let critical_points = Seq.ints(0) |> Seq.take(Array.length(block))
|> Seq.product(_, Seq.ints(0) |> Seq.take(Array.length(block[0])))
|> Seq.filter(((xx, yy)) => {
  (xx + 1 >= Array.length(block[yy]) || block[yy][xx + 1] == '#') && (block[yy][xx] == '.' || block[yy][xx] == 'O')
})
Seq.memoize;

// A signature is the number of boulders at each critical point; it
// uniquely identifies a block state after an eastward shift
module Signature = {
  type t = list(int)

  let compare(sig1, sig2) = {
    switch (sig1, sig2) {
    | ([head1, ...rest1], [head2, ...rest2]) when head1 == head2 => compare(rest1, rest2)
    | ([head1, ..._], [head2, ..._]) => Int.compare(head1, head2)
    | ([], [_, ..._]) => -1
    | ([_, ..._], []) => 1
    | ([], []) => 0
    }
  }

  let of_block(block) = {
    critical_points |> Seq.map(((xx, yy)) => {
      let rec loop(xx', count) = {
        if (xx' >= 0 && block[yy][xx'] == 'O') {
          loop(xx' - 1, count + 1)
        } else {
          count
        }
      }

      loop(xx, 0)
    })
    |> List.of_seq
  }
}

module Visited = Map.Make(Signature)

let rec loop(idx, visited) = {
  if (idx >= 1_000_000_000) {
    weight(block)
  } else {
    let signature = Signature.of_block(block)
    let (skip, visited') = switch (visited |> Visited.find_opt(signature)) {
    | Some(iterations) =>
      let period = idx - iterations;
      ((999_999_999 - idx) / period * period, visited);
    | None => (0, visited |> Visited.add(signature, idx))
    }

    if (skip > 0) {
      loop(idx + skip, visited')
    } else {
      // if (idx mod 50_000 == 0) { print_int(idx); print_newline() }
      shift_in_place(`North, block);
      shift_in_place(`West, block);
      shift_in_place(`South, block);
      shift_in_place(`East, block);
      loop(idx + 1, visited')
    }
  }
}
print_endline("Weight 2: " ++ string_of_int(loop(0, Visited.empty)))
// block |> Array.iter(arr => {arr |> Array.iter(char => print_char(char)); print_newline()})

