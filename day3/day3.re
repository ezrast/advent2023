// Part one
let lines = In_channel.input_lines(stdin) |> Array.of_list
let width = lines |> Array.length
let depth = lines[0] |> String.length

let try_get(xx, yy) = {
  try(lines[xx] |> String.get(_, yy)) { | _ => '.' };
}

let is_digit(xx, yy) = {
  let chr = try_get(xx, yy);
  chr >= '0' && chr <= '9'
}

let is_symbol(xx, yy) = {
  let chr = try_get(xx, yy);
  chr != '.' && (chr < '0' || chr > '9')
}

let get_num_starting_at(xx, yy) = {
  let rec loop(y', length) = {
    if (is_digit(xx, y')) {
      loop(y' + 1, length + 1)
    } else {
      length
    }
  }
  let length = loop(yy, 0)
  let substr = lines[xx] |> String.sub(_, yy, length)
  int_of_string(substr)
}

let is_near_symbol(xx, yy) = {
  [-1, 0, 1] |> List.exists(dx => {
    [-1, 0, 1] |> List.exists(dy => {
      is_symbol(xx + dx, yy + dy)
    })
  })
}

let rec find_first_digit(xx, yy) = {
  if (is_digit(xx, yy)) {
    switch (find_first_digit(xx, yy - 1)) {
    | Some((x', y')) => Some((x', y'))
    | None => Some((xx, yy))
    }
  } else {
    None
  }
}

let compare_points((x1,y1), (x2,y2)) = {
  if (x1 == x2) {
    Int.compare(y1, y2)
  } else {
    Int.compare(x1, x2)
  }
}

let every_pair = Seq.init(width, Fun.id) |> Seq.map(xx => {
  Seq.init(depth, Fun.id) |> Seq.map(yy => (xx, yy))
}) |> Seq.concat

let part_number_coordinates = every_pair |> Seq.filter_map(((xx, yy)) => {
  if (is_near_symbol(xx, yy)) {
    find_first_digit(xx, yy)
  } else {
    None
  }
})
|> List.of_seq
|> List.sort_uniq(compare_points)

let part_numbers = part_number_coordinates |> List.map(((xx, yy)) => get_num_starting_at(xx, yy))
let part_numbers_sum = part_numbers |> List.fold_left((+), 0)
print_endline("Part numbers: " ++ string_of_int(part_numbers_sum))

// Part two

let find_adj_numbers(xx, yy) {
  [
    (xx - 1, yy - 1), (xx - 1, yy), (xx - 1, yy + 1),
    (xx    , yy - 1),               (xx    , yy + 1),
    (xx + 1, yy - 1), (xx + 1, yy), (xx + 1, yy + 1),
  ]
  |> List.filter_map(((x', y')) => find_first_digit(x', y'))
  |> List.sort_uniq(compare_points)
}

let gear_ratios = every_pair |> Seq.filter_map(((xx, yy)) => {
  if (try_get(xx, yy) != '*') {
    None
  } else {
    switch (find_adj_numbers(xx, yy)) {
    | [(x1, y1), (x2, y2)] =>
      Some(get_num_starting_at(x1, y1) * get_num_starting_at(x2, y2))
    | _ => None
    }
  }
})

let gear_ratio_sum = gear_ratios |> Seq.fold_left((+), 0)
print_endline("Gear ratios: " ++ string_of_int(gear_ratio_sum))
