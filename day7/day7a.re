// Part 1

type hand = {
  cards: Seq.t(int),
  bid: int,
}

let char_to_rank(char) = "23456789TJQKA"
|> String.to_seq
|> Seq.find_mapi((idx, candidate) => if (candidate == char) Some(idx) else None)
|> Option.get

let hand_strength(cards) = {
  let rank_counts = Array.make(13, 0)
  cards |> Seq.iter(rank => rank_counts[rank] = rank_counts[rank] + 1)
  rank_counts |> Array.sort((aa, bb) => Int.compare(bb, aa))

  switch (rank_counts[0], rank_counts[1]) {
  | (5, 0) => 6
  | (4, 1) => 5
  | (3, 2) => 4
  | (3, 1) => 3
  | (2, 2) => 2
  | (2, 1) => 1
  | (1, 1) => 0
  | _ => failwith("Impossible")
  }
}

let cmp_hand(h1, h2) {
  let strength1 = hand_strength(h1.cards);
  let strength2 = hand_strength(h2.cards);
  if (strength1 == strength2) {
    Seq.compare(Int.compare, h1.cards, h2.cards)
  } else {
    strength1 - strength2
  }
}

let parse_line(line) = {
  switch (line |> String.split_on_char(' ')) {
  | [cards_str, bid_str] =>
    assert(String.length(cards_str) == 5);
    {
      cards: cards_str |> String.to_seq |> Seq.map(char_to_rank) |> Seq.memoize,
      bid: int_of_string(bid_str),
    }
  | _ => failwith("Bad line: " ++ line)
  }
}

let run(lines) = {
  let sorted_hands = lines |> List.map(parse_line) |> List.sort(cmp_hand)
  let scores = sorted_hands |> List.mapi((idx, hand) => { (idx + 1) * hand.bid })
  let score_sum = scores |> List.fold_left((+), 0)
  print_endline("Score sum: " ++ string_of_int(score_sum))
}
