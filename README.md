Advent of Code 2023: https://adventofcode.com

Solutions in [Reason](https://opam.ocaml.org/packages/reason/)-flavored OCaml.
Using the minimalist standard library instead of Batteries or Core, so expect verbosity unrepresentative of OCaml as she is spoke.

Instructions:

* get [opam](https://opam.ocaml.org)
* install dependencies:
  * globally: `opam install --deps-only .`
  * locally: `opam switch create --deps-only .`
* optionally, install dev tools: `opam install ocaml-lsp-server ocamlformat`
* build: `opam exec dune build`
* run: `./_build/default/day1/day1.exe`, etc
