let lines = In_channel.input_lines(stdin)

let parse1(lines) = {
  lines |> List.map(line => {
    switch (line |> String.split_on_char(' ')) {
    | [pattern, nums_str] =>
      let nums = nums_str
      |> String.split_on_char(',')
      |> List.map(int_of_string);

      let pattern = pattern
      |> String.split_on_char('.') |> List.filter((!=)("")) |> String.concat(".");

      (
        pattern,
        nums |> List.to_seq
      )
    | _ => failwith("Bad line")
    }
  })
}

let parse2(lines) = {
  lines |> List.map(line => {
    switch (line |> String.split_on_char(' ')) {
    | [pattern, nums_str] =>
      let nums = nums_str
      |> String.split_on_char(',')
      |> List.map(int_of_string);

      let pattern = (pattern ++ "?" ++ pattern ++ "?" ++ pattern ++ "?" ++ pattern ++ "?" ++ pattern)
      |> String.split_on_char('.') |> List.filter((!=)("")) |> String.concat(".");

      (
        pattern,
        List.concat([nums, nums, nums, nums, nums]) |> List.to_seq
      )
    | _ => failwith("Bad line")
    }
  })
}

let run_lengths(str) = {
  assert(String.index_opt(str, '#') == None)
  str |> String.split_on_char('.') |> List.filter((!=)("")) |> List.map(String.length)
}

let rec m_choose_n(mm, nn) = {
  if (nn > mm) 0
  else if (nn > mm / 2) m_choose_n(mm, mm - nn)
  else {
    (Seq.ints(mm - nn + 1) |> Seq.take(nn) |> Seq.fold_lefti((acc, idx, num) => acc * num / (1 + idx), 1))
  }
};

let rec calculate_runs(~depth=1, run_lengths, nums) = {
  // Once str is all sequences of '?'
  if (Seq.fold_left((+), 0, nums) > List.fold_left((+), 0, run_lengths)) 0
  else if (Seq.is_empty(nums)) 1
  else {
    switch run_lengths {
    | [] => 0
    | [first, ...rest] =>
      Seq.cons(0, nums) |> Seq.mapi((idx, _num) => {
        let num_split_a = nums |> Seq.take(idx)
        let num_split_b = nums |> Seq.drop(idx)
        let left = m_choose_n(1 + first - Seq.fold_left((+), 0, num_split_a), Seq.length(num_split_a))
        let right = if (left == 0) 0 else calculate_runs(~depth=depth+1, rest, num_split_b)

        left * right
      })
      |> Seq.fold_left((+), 0)
    }
  }
}

let rec calculate(~depth=0, str, nums: Seq.t(int)) = {
  switch (String.index_opt(str, '#')) {
  | Some(hash_idx) =>
    nums |> Seq.mapi((num_idx, num) => {
      Seq.ints(0)
      |> Seq.take(num)
      |> Seq.filter_map(pos => {
        let slice_start = hash_idx - pos
        let slice_end = slice_start + num
        if (slice_start < 0) None
        else if (slice_end > String.length(str)) None
        else if (!(Seq.ints(slice_start) |> Seq.take_while((<)(_, slice_end)) |> Seq.for_all(idx => String.get(str, idx) != '.'))) None
        else if (slice_start > 0 && String.get(str, slice_start - 1) == '#') None
        else if (slice_end < String.length(str) && String.get(str, slice_end) == '#') None
        else {
          let str_split_a = if (slice_start == 0) "." else String.sub(str, 0, slice_start - 1)
          let str_split_b = if (slice_end == String.length(str)) "." else String.sub(str, slice_end + 1, String.length(str) - slice_end - 1)

          let num_split_a = nums |> Seq.take(num_idx);
          let num_split_b = nums |> Seq.drop(num_idx + 1);

          let left = calculate(~depth=depth+1, str_split_a, num_split_a)
          let right = if (left == 0) 0 else { calculate(~depth=depth+1, str_split_b, num_split_b) }

          Some(left * right)
        };
      })
      |> Seq.fold_left((+), 0)
    })
    |> Seq.fold_left((+), 0)
  | None =>
    calculate_runs(run_lengths(str), nums)
  }
}

let sum(input) = input |> List.mapi((_idx, line) => {
  let xx = calculate(fst(line), snd(line))
  // print_endline("Idx " ++ string_of_int(idx) ++ ", " ++ fst(line) ++ ": " ++ string_of_int(xx));
  xx
})
|> List.fold_left((+), 0)

// let sum = output |> List.fold_left((+), 0)
print_endline("Sum 1: " ++ string_of_int(sum(parse1(lines))))
print_endline("Sum 2: " ++ string_of_int(sum(parse2(lines))))
