let line = In_channel.input_all(stdin)

let hash = String.fold_left((acc, char) => {
  if (char == '\n') acc else
  ((acc + Char.code(char)) * 17) land 255
}, 0)

let codes = line |> String.split_on_char(',')
let hashes = codes |> List.map(hash)
let hash_sum = hashes |> List.fold_left((+), 0)
print_endline("Hash sum: " ++ string_of_int(hash_sum))

// Part two

type instruction_type =
| Remove
| Add(int)

let parse_instruction(str) = {
  let str = if (String.ends_with(~suffix="\n", str)) String.sub(str, 0, String.length(str) - 1) else str
  if (String.ends_with(~suffix="-", str)) {
    let label = String.sub(str, 0, String.length(str) - 1);
    (label, Remove)
  } else {
    switch (String.split_on_char('=', str)) {
    | [label, value] => (label, Add(int_of_string(value)))
    | _ => failwith("Bad instruction: " ++ str)
    }
  }
}

let hash_map = Array.make(256, [])
let hash_bucket_add(label, value, bucket) = {
  let seen = ref(false)
  let result = bucket |> List.map(((label', value')) => {
    if (label == label') {
      seen := true;
      (label, value)
    } else {
      (label', value')
    }
  });
  if (seen^) result else [(label, value), ...bucket]
}

let hash_bucket_remove(label, bucket) = {
  bucket |> List.filter(((label', _value)) => { label' != label })
}

let instructions = codes |> List.map(parse_instruction)
instructions |> List.iter(((label, instr_type)) => {
  let hash_value = hash(label)
  hash_map[hash_value] = hash_map[hash_value] |> switch instr_type {
  | Remove => hash_bucket_remove(label)
  | Add(value) => hash_bucket_add(label, value)
  }
})

// let string_of_lens((label, int)) = label ++ ":" ++ string_of_int(int);

// Seq.ints(0) |> Seq.take(256) |> Seq.iter(idx => {
//   if (!List.is_empty(hash_map[idx])) {
//     print_int(idx)
//     print_string(": ")
//     print_string(hash_map[idx] |> List.map(string_of_lens) |> String.concat(", "))
//     print_newline()
//   }
// })

let bucket_focusing_power(idx) = {
  let folder(acc, idx2, (_label, value)) = {
    acc + (
      (1 + idx)
      * (1 + idx2)
      * value
    )
  };
  hash_map[idx] |> List.rev |> List.to_seq |> Seq.fold_lefti(folder, 0)
}

let total_focusing_power = Seq.ints(0) |> Seq.take(256) |> Seq.fold_left((acc, idx) => acc + bucket_focusing_power(idx), 0)
print_endline("Total focusing power: " ++ string_of_int(total_focusing_power))
